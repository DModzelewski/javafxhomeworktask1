package main;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;

import java.util.Random;

public class MainScene extends Application {
    public static void main(String[] args){
        launch(args);
    }
    @Override
    public void start(Stage stage) throws Exception {
        Group root = new Group();

        Polygon star = new Polygon(300,200,271,260,205,269,252,315,241,381,300,350,359,381,
                348,315,395,269,329,260 );
        star.setFill(Color.BLUE);
        root.getChildren().add(star);

        Label text = new Label("HOMEWORKS ARE AWESOME");
        text.setTextFill(Color.GREEN);
        text.setLayoutX(200);
        text.setLayoutY(150);
        root.getChildren().add(text);

        //Okrąg o średnicy 200, tak dla potwierdzenia!
        Circle circle = new Circle(100, Color.WHITE);
        circle.setCenterX(300);
        circle.setCenterY(300);
        circle.setOpacity(0.1);
        root.getChildren().add(circle);

        Shape[] border1 = drawBorder(600, 1);
        Shape[] border2 = drawBorder(600, 2);
        Shape[] border3 = drawBorder(600, 3);
        Shape[] border4 = drawBorder(600, 4);
        root.getChildren().addAll(border1);
        root.getChildren().addAll(border2);
        root.getChildren().addAll(border3);
        root.getChildren().addAll(border4);


        Scene scene = new Scene(root, 600, 600, Color.BLACK);
        stage.setScene(scene);
        stage.show();
    }

    public static Shape[] drawBorder(double res, int side){
        Random random = new Random();
        double diameter = 50;
        Shape[] border = new Shape[(int)(res/diameter-1)];
        if(side==1) {
            for (int i = 0; i < border.length;i++ ) {
                if(i%2==1){
                    border[i] = new Rectangle(diameter, diameter, Color.color(random.nextDouble(),
                            random.nextDouble(), random.nextDouble()));
                    border[i].setLayoutX(i * diameter);
                }else{
                    border[i] = new Circle(diameter / 2, Color.color(random.nextDouble(),
                            random.nextDouble(), random.nextDouble()));
                    border[i].setLayoutX(i * diameter + 0.5 * diameter);
                    border[i].setLayoutY(diameter / 2);
                }
            }
        }
        if(side==2) {
            for (int i = 0; i < border.length; i++ ) {
                if(i%2==1){
                    border[i] = new Circle(diameter / 2, Color.color(random.nextDouble(),
                            random.nextDouble(), random.nextDouble()));
                    border[i].setLayoutY(i * diameter + 0.5 * diameter);
                    border[i].setLayoutX(res-(diameter / 2));
                }else{
                    border[i] = new Rectangle(diameter, diameter, Color.color(random.nextDouble(),
                            random.nextDouble(), random.nextDouble()));
                    border[i].setLayoutY(i * diameter);
                    border[i].setLayoutX(res-(diameter));
                }
            }
        }
        if(side==3){
            for (int i = 0; i < border.length;i++ ) {
                if(i%2==1){
                    border[i] = new Rectangle(diameter, diameter, Color.color(random.nextDouble(),
                            random.nextDouble(), random.nextDouble()));
                    border[i].setLayoutX(i * diameter+diameter);
                    border[i].setLayoutY(600-diameter);
                }else{
                    border[i] = new Circle(diameter / 2, Color.color(random.nextDouble(),
                            random.nextDouble(), random.nextDouble()));
                    border[i].setLayoutX(i * diameter + (0.5 * diameter)*3);
                    border[i].setLayoutY(600-(diameter / 2));
                }
            }
        }
        if(side==4) {
            for (int i = 0; i < border.length; i++ ) {
                if(i%2==1){
                    border[i] = new Circle(diameter / 2, Color.color(random.nextDouble(),
                            random.nextDouble(), random.nextDouble()));
                    border[i].setLayoutY(i * diameter + (0.5 * diameter)*3);
                    border[i].setLayoutX(diameter / 2);
                }else{
                    border[i] = new Rectangle(diameter, diameter, Color.color(random.nextDouble(),
                            random.nextDouble(), random.nextDouble()));
                    border[i].setLayoutY(i * diameter+diameter);
                }
            }
        }
        return border;
    }

}
